import { test } from "@b08/test-runner";

test("two files bucket", async expect => {
  // arrange
  const srcFile = "../testData/twoFiles";
  const target = await import(srcFile);

  // act
  const result1 = target.func2();
  const result2 = target.func3();

  // assert
  expect.equal(result1, 2);
  expect.equal(result2, 3);
});

