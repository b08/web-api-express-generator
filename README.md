# @b08/generator-seed
This is a seed for typescript generator, includes basic amenities like build an lint, same as @b08/library-seed, except building and testing with es5 and es6 targets. This is because generators are not supposed to be used in live projects, only during design time. \
Tasks are extracted to @b08/generator-tasks package.

# Features
1. test task copies testDataSrc files to testData folder and deletes old data from it prior to executing tests. Generator is supposed to be executed within testData subfolders.
2. There are examples of how to build generated test code.
3. Tests can be executed in parallel, since building generated code can take some time. Update test-runner.json to specify threads count.
4. "razor" and "auto" tasks for running on razor files manually and automatically.

# Creating a generator from this seed
Same as @b08/library-seed, use @b08/seed-that to start new generator off this seed.

# Limitation
To reduce build times, test data generation (including generated files build) is executed in parallel with build.
To collect coverage of data generation, data is regenerated the second time while running the tests.
