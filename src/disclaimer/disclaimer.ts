import { GeneratorOptions } from "../types/generatorOptions.type";

export function disclaimer(options: GeneratorOptions): string {
  const generatorName = "@b08/generator-seed";
  return `// warning! This code was generated by ${generatorName}${options.linefeed}` +
    `// manual changes to this file will be overwritten next time the code is regenerated.${options.linefeed}`;

}
